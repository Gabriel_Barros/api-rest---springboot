create table public.pessoa(
    codigo bigserial primary key,
    nome varchar(30) not null,
    logradouro varchar(20),
    numero varchar(5),
    complemento varchar(10),
    bairro varchar(15),
    cep varchar(9),
    cidade varchar(20),
    estado varchar(15),
    ativo boolean default true
);

insert into public.pessoa(nome, logradouro, numero, complemento, bairro, cep, cidade, estado)
values
('Gabriel Barros', 'Rua Castro Alves', '153', '', 'Aleixo', '69060040', 'Manaus', 'Amazonas'),
('Raquel Barbosa', 'Av.Castelo Branco', '1028', 'B', 'Cachoeirinha', '69040-011', 'Manaus', 'Amazonas'),
('Anderson Donald', 'Rua 3', '1234', '', 'Cachoeirinha', '69057-032', 'Manaus', 'Amazonas');

insert into public.pessoa(nome)
values
('Lucynthia Maduro'),
('Claudia Franceschi');

