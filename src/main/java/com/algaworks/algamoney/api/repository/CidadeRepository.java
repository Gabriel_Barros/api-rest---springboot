package com.algaworks.algamoney.api.repository;

import com.algaworks.algamoney.api.model.Cidade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CidadeRepository extends JpaRepository<Cidade, Long>{

    public List<Cidade> findByEstadoCodigo(Long estadoCodigo);
}
